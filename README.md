# Hands on AWS IoT Core

## Pre requisites

Install [**Terraform**](https://www.terraform.io/downloads)

Have an AWS User with the following policies:

* IAMFullAccess
* AmazonS3FullAccess
* AmazonDynamoDBFullAccess
* AWSIoTFullAccess


First we'll need to retrieve the AWS Root certificate:

```shell
mkdir files
```

then:

```shell
curl https://www.amazontrust.com/repository/AmazonRootCA1.pem > files/root-CA.crt
```

Then we have to install the python dependencies with:

```shell
pip install -r requirements.txt
```

We also have to initialize terraform with the command (execute it in the *terraform* directory :

```shell
terraform init
```


## Part 1: IoT Thing and certificates

![img2.png](doc/img2.png)


**hint**

I provided in the *output.tf* file some outputs to create the certificate related files so try to name your certificate 'cert' to avoid problems
 

### First we'll need to declare some *AWS Iot Core resources* in the *iot.tf* file :

[**aws_iot_certificate**](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iot_certificate) with the *active* option without CSR

[**aws_iot_policy**](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iot_policy) with the policy located in the *terraform/files/iot_policy.json* file

[**aws_iot_policy_attachment**](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iot_policy_attachment) to attach the policy declared before to our certificate

[**aws_iot_thing**](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iot_thing) which is our connected object

[**aws_iot_thing_principal_attachment**](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iot_thing_principal_attachment) to attach our policy to our *iot_thing*


![img3.png](doc/img3.png)


I declared some *resources* and *outputs* in the output.ff file so make sure the names match the one you use in your resources


### Then we have to declare a *data source*:

[**aws_iot_endpoint**](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iot_endpoint) with an endpoint type "iot:Data-ATS"


### Now we can create our resources with the command:

```shell
terraform apply
```

### Finally we can test the communication between our iot_thing and AWS IoT Core

Go to the AWS Console in the IoT Core service in the section **Test** => *MQTT test client* and enter "test/topic" in the **Subscribe to a topic** tab then click on the *Subscribe* button 

![img.png](doc/img.png)

Now run the following command (replace "<AWS_IOT_ENDPOINT>" by the "iot_endpoint" printed in the console after you used *terraform apply*) 

```shell
python pub_sub.py --endpoint <AWS_IOT_ENDPOINT> --ca_file files/root-CA.crt --cert files/test.cert.pem --key files/test.private.key
```

The command will send some MQTT messages to AWS IoT Core within the "test/topic" topic and you'll see the messages in the AWS Console:

![img1.png](doc/img1.png)


## Part 2: Simple IoT rule DynamoDB

![img4.png](doc/img4.png)

In this part we are going to add:

an [**aws_dynamodb_table**](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) named "Temperature" with a hash_key named "Id" an attribute named "Id" of type string

an [**aws_iot_topic_rule**](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iot_topic_rule) with the sql query "SELECT *, timestamp() as timestamp FROM 'test/topic'" and a dynamodbv2 action to link to the temperature table

* an [**aws_iam_role**](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) called **iot_role** with the following *assume_role_policy*:
```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "iot.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
```

an [**aws_iam_role_policy**](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) to grant dynamoDB (**dynamodb:PutItem** action) access to the iam role "iot_role"  

to forward our messages to a DynamoDB table 

## Part 3: conditional IoT rule s3 

![img5.png](doc/img5.png)

Now we are going to add a second topic rule to write an object into a s3 bucket when the temperature is above 35 °C

an [**aws_s3_bucket**](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) with the *force_destroy* to true

an [**aws_iam_role_policy**](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) to grant s3 (**s3:PutObject** action) access to the iam role "iot_role" 

an [**aws_iot_topic_rule**](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iot_topic_rule) with the sql query: "SELECT *, timestamp() as timestamp FROM 'test/topic' where temperature > 35" and a s3 action to link to the s3 bucket
with a key : "test-topic/$${timestamp()}.txt"


## Part 4: IoT Events

https://docs.aws.amazon.com/iotevents/latest/developerguide/iotevents-getting-started.html